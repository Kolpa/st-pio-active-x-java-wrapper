package org.bitbucket.Kolpa.PIO;

import com4j.*;

/**
 * Defines methods to create COM objects
 */
public abstract class ClassFactory {
  private ClassFactory() {} // instanciation is not allowed
  
  /**
   * PIO24 Control
   */
  public static _DPIO24II createPIO24II() {
    return COM4J.createInstance(_DPIO24II.class, "{35CC0EDF-0522-11D2-95A5-006097B81D3E}" );
  }
}
