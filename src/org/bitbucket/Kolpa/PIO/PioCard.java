package org.bitbucket.Kolpa.PIO;

public class PioCard {
	private _DPIO24II card;

	PioCard() {
		card = ClassFactory.createPIO24II();
	}

	int getPort(short port) {
		return card.port(port);
	}

	void setPort(short port, int rhs){
		card.port(port, rhs);
	}
	
	int getLine(short port, short line) {
		return card.getLine(port, line);
	}
	
	void setLine(short port, short line) {
		card.setLine(port, line);
	}
	
	void resetLine(short port, short line) {
		card.resetLine(port, line);
	}
	
	void aboutBox() {
		card.aboutBox();
	}
	
	IO getDirPort1() {
		return card.getDirPort1();
	}
	
	void setDirPort1(IO newValue) {
		card.setDirPort1(newValue);
	}
	
	IO getDirPort2() {
		return card.getDirPort2();
	}
	
	void setDirPort2(IO newValue) {
		card.setDirPort2(newValue);
	}
	
	IO getDirPort3H() {
		return card.getDirPort3H();
	}
	
	void setDirPort3H(IO newValue) {
		card.setDirPort3H(newValue);
	}
	
	IO getDirPort3L() {
		return card.getDirPort3L();
	}
	
	void setDirPort3L(IO newValue) {
		card.setDirPort3L(newValue);
	}
	
	int getInstalledCards() {
		return card.getInstalledCards();
	}
		
	int getCardId() {
		return card.getCardId();
	}
	
}
