package org.bitbucket.Kolpa.PIO;

import com4j.*;

/**
 * Dispatch interface for PIO24 Control
 */
@IID("{00020400-0000-0000-C000-000000000046}")
public interface _DPIO24II extends Com4jObject {
  // Methods:
  /**
   * @param port Mandatory short parameter.
   * @param line Mandatory short parameter.
   */

  @DISPID(7)
  short getLine(
    short port,
    short line);


  /**
   * @param port Mandatory short parameter.
   * @param line Mandatory short parameter.
   */

  @DISPID(8)
  void setLine(
    short port,
    short line);


  /**
   * @param port Mandatory short parameter.
   * @param line Mandatory short parameter.
   */

  @DISPID(9)
  void resetLine(
    short port,
    short line);


  /**
   * <p>
   * Getter method for the COM property "Port"
   * </p>
   * @param port Mandatory short parameter.
   */

  @DISPID(10)
  @PropGet
  int port(
    short port);


  /**
   * <p>
   * Setter method for the COM property "Port"
   * </p>
   * @param port Mandatory short parameter.
   * @param rhs Mandatory int parameter.
   */

  @DISPID(10)
  @PropPut
  void port(
    short port,
    int rhs);


  /**
   */

  @DISPID(-552)
  void aboutBox();


  // Properties:
  /**
   * <p>
   * Getter method for the COM property "DirPort1"
   * </p>
   * @return The COM property DirPort1 as a org.bitbucket.IO
   */
  @DISPID(1)
  @PropGet
  IO getDirPort1();

  /**
   * <p>
   * Setter method for the COM property "DirPort1"
   * </p>
   * @param newValue The new value for the COM property DirPort1 as a org.bitbucket.IO
   */
  @DISPID(1)
  @PropPut
  void setDirPort1(IO newValue);

  /**
   * <p>
   * Getter method for the COM property "DirPort2"
   * </p>
   * @return The COM property DirPort2 as a org.bitbucket.IO
   */
  @DISPID(2)
  @PropGet
  IO getDirPort2();

  /**
   * <p>
   * Setter method for the COM property "DirPort2"
   * </p>
   * @param newValue The new value for the COM property DirPort2 as a org.bitbucket.IO
   */
  @DISPID(2)
  @PropPut
  void setDirPort2(IO newValue);

  /**
   * <p>
   * Getter method for the COM property "DirPort3L"
   * </p>
   * @return The COM property DirPort3L as a org.bitbucket.IO
   */
  @DISPID(3)
  @PropGet
  IO getDirPort3L();

  /**
   * <p>
   * Setter method for the COM property "DirPort3L"
   * </p>
   * @param newValue The new value for the COM property DirPort3L as a org.bitbucket.IO
   */
  @DISPID(3)
  @PropPut
  void setDirPort3L(IO newValue);

  /**
   * <p>
   * Getter method for the COM property "DirPort3H"
   * </p>
   * @return The COM property DirPort3H as a org.bitbucket.IO
   */
  @DISPID(4)
  @PropGet
  IO getDirPort3H();

  /**
   * <p>
   * Setter method for the COM property "DirPort3H"
   * </p>
   * @param newValue The new value for the COM property DirPort3H as a org.bitbucket.IO
   */
  @DISPID(4)
  @PropPut
  void setDirPort3H(IO newValue);

  /**
   * <p>
   * Getter method for the COM property "InstalledCards"
   * </p>
   * @return The COM property InstalledCards as a int
   */
  @DISPID(5)
  @PropGet
  int getInstalledCards();

  /**
   * <p>
   * Setter method for the COM property "InstalledCards"
   * </p>
   * @param newValue The new value for the COM property InstalledCards as a int
   */
  @DISPID(5)
  @PropPut
  void setInstalledCards(int newValue);

  /**
   * <p>
   * Getter method for the COM property "CardId"
   * </p>
   * @return The COM property CardId as a int
   */
  @DISPID(6)
  @PropGet
  int getCardId();

  /**
   * <p>
   * Setter method for the COM property "CardId"
   * </p>
   * @param newValue The new value for the COM property CardId as a int
   */
  @DISPID(6)
  @PropPut
  void setCardId(int newValue);

}
